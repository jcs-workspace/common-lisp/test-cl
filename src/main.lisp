(defpackage #:basic-test
  (:use :cl))

(in-package #:basic-test)

(ql:quickload :clack)
(ql:quickload :ningle)

;; To be able to load hunchentoot without openSSL
;;(push :hunchentoot-no-ssl *features*)

(defvar app (make-instance 'ningle:app))

(setf (ningle:route app "/hello") "hey there!!!")

(defun main()
  (clack:clackup app :port 5500)
  (loop))
