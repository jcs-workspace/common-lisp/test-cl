# test-cl

Try out Common Lisp, following a youtube video
[Creating Your First Lisp Project - Quicklisp, asdf, and Packages](https://www.youtube.com/watch?v=LqBbGFMPcDI&t=1115s&ab_channel=GavinFreeborn).

```
(pushnew (uiop:getcwd) ql:*local-project-directories*)
```
