(defsystem "basic-test"
  :depends-on (#:clack
               #:ningle
               #:clack-handler-hunchentoot)
  :components ((:module "src"
                :components
                ((:file "main"))))
  :build-operation "program-op"
  :build-pathname "basic-rest"
  :entry-point "basic-test::main")
